FROM openjdk:11

WORDIR /app
COPY . /app
RUN mvn install -Dmaven.test.skip=true

CMD ["java", "-jar", "GetThingsDone-app/target/GetThingsDone-app-1.0-SNAPSHOT.jar"]